const assert = require('assert');
const app = require('../../src/app');

describe('\'contacts-entities\' service', () => {
  it('registered the service', () => {
    const service = app.service('contacts-entities');

    assert.ok(service, 'Registered the service');
  });
});
