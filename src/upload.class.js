'use strict';
const { Forbidden, NotAcceptable, Unprocessable } = require('feathers-errors');
var path = require('path');
const fs = require('fs');
const im = require('imagemagick');

const multer = require('multer');

exports.Upload = class Upload {

  constructor(req, res, app, typeUpload, inputName, fileUse, maxNumberFiles, maxSize) {
    this.typeUpload = typeUpload || 'image'; // This can be document || image; We don't accept anything else beside this 2! maxSize
    this.inputName = inputName || 'file'; // If this is not set, then this input have name file!
    this.fileUse = fileUse || ('avatar');
    this.maxNumberFiles = maxNumberFiles || 1; // If this is not set, then you can upload only 1 file!
    this.maxSize = maxSize || false; // If this is not set, then you are based on number of files, not the size of file/s!
    this.req = false;
    this.res = false;
    this.app = false;
    this.setupReq(req);
    this.setupRes(res);
    this.setupApp(app);
    // console.log('We are in upload', this.req);
    this.setDirectoryPath()
  }

  setDirectoryPath() {
    this.appDir = path.dirname(require.main.filename);
    this.dir = this.appDir + '/uploads';
    this.getPath();
    this.storageMulter();
    this.fileFilterMulter();
  }

  getPath(){
    let pathCreate = '';
    let params = this.req.params;
    let type = '/' + this.typeUpload + 's'; // '/images' : '/documents';
    if (params) {
      if (params.user_id && !params.complex_id) {
        pathCreate = this.dir + '/users/' + params.user_id + type;
      }
      if (params.complex_id && params.complex_id !== 'all') {
        pathCreate = this.dir + '/complex/' + params.complex_id + (!params.building_id ? type : '');

        if (params.building_id) {
          pathCreate = pathCreate + '/building' + (params.building_id === 'all' ? type : '/' + params.building_id + (!params.flat_id ? type : ''));

          if (params.flat_id) {
            pathCreate = pathCreate + '/flats' + (params.flat_id === 'all' ? type : '/' + params.flat_id + type)
          }
        }
      } else if (params.complex_id && params.complex_id === 'all') {
        pathCreate = this.dir + '/complex' + type
      }

      if ( pathCreate.length === 0) return false;
      this.createPath(pathCreate);
      return pathCreate
    }

    return false;
  };

  createPath(targetDir, { isRelativeToScript = false } = {}) {
    const sep = path.sep;
    const initDir = path.isAbsolute(targetDir) ? sep : '';
    const baseDir = isRelativeToScript ? __dirname : '.';

    return targetDir.split(sep).reduce((parentDir, childDir) => {
      const curDir = path.resolve(baseDir, parentDir, childDir);
      try {
        fs.mkdirSync(curDir);
      } catch (err) {
        if (err.code === 'EEXIST') { // curDir already exists!
          return curDir;
        }

        // To avoid `EISDIR` error on Mac and `EACCES`-->`ENOENT` and `EPERM` on Windows.
        if (err.code === 'ENOENT') { // Throw the original parentDir error on curDir `ENOENT` failure.
          throw new Error(`EACCES: permission denied, mkdir '${parentDir}'`);
        }

        const caughtErr = ['EACCES', 'EPERM', 'EISDIR'].indexOf(err.code) > -1;
        if (!caughtErr || caughtErr && curDir === path.resolve(targetDir)) {
          throw err; // Throw if it's just the last created dir.
        }
      }

      return curDir;
    }, initDir);
  }

  storageMulter() {
    const dir = this.getPath();

    this.storage = multer.diskStorage({
      destination: function (req, file, cb) {
        if (!dir && !fs.existsSync(dir)) {
          throw new ForbiddenError('No No No')
        }
        cb(null, dir)
      },
      filename: function (req, file, cb) {
        cb(null, `${new Date().getTime()}_${file.originalname}`)
      }
    });
  }

  fileFilterMulter() {
    var self = this;
    const checkFileExtension = function (originalNameWithExtension) {

      if (self.typeUpload === 'image') {
        if (!originalNameWithExtension.match(/\.(jpg|JPG|jpeg|JPEG|png|PNG|gif|GIF)$/)) {
          return { msj: 'Only image files are allowed!', state: false };
        }
        return { msj: '', state: true }
      } else if (self.typeUpload === 'document') {
        if (!originalNameWithExtension.match(/\.(pdf|PDF|doc|DOC|docx|DOCX|txt|rtf|RTF|wks|WKS|wps|WPS|wpd|WPD|xlr|XLR|xls|XLS|xlsx|XLSX|csv|odt|ODT|odp|ODP|ods|ODS|pps|PPS|ppt|PPT|pptx|PPTX)$/)) {
          return { msj: 'Only document files are allowed!', state: false };
        }
        return { msj: '', state: true }
      }
    };
    this.fileFilter = async (req, file, cb) => {
      // reject a file if dont have extension
      let check = await checkFileExtension(file.originalname);
      if (!check.state) {
        req.fileValidationError = check.msj;
        cb(check.msj, false);
      }
      cb(null, true);
    };
  }

  setupApp(app) {
    if (app) {
      this.app = app;
      return this.app;
    }
    throw new Forbidden(`You are not allowed to do this action!`);
  };

  setupReq(req) {
    if (req) {
      this.req = req;
      return this.req;
    }
    throw new Forbidden(`You are not allowed to do this action!`);
  };

  setupRes(res) {
    if (res) {
      this.res = res;
      return this.res;
    }
    throw new Forbidden(`You are not allowed to do this action!`);
  }

  async uploadStart() {
    var multipartMiddleware;
    if (this.maxNumberFiles > 1) {
      multipartMiddleware = multer({
        storage: this.storage,
        limits: {
          fileSize: 1024 * 1024 * 10
        },
        fileFilter: this.fileFilter
      }).array(this.inputName, this.maxNumberFiles)
    }
    else {
      multipartMiddleware = multer({
        storage: this.storage,
        limits: {
          fileSize: 1024 * 1024 * 10
        },
        fileFilter: this.fileFilter
      }).single(this.inputName)
    }

    let req = this.req;
    let res = this.res;
    let app = this.app;

    if (this.fileUse === 'avatar') {
      let documentsForDelete = await app.service('documents').find({
        query: {
          owner_user_id: req.params.user_id,
          description: this.fileUse,
          folder: 'images'
        }
      });
      console.log(documentsForDelete);

      if (documentsForDelete && documentsForDelete.length > 0) {
        for (const objectFile of documentsForDelete) {
          let dirForDelete = this.getPath();
          let filesToDelete = this.getAllFiles(dirForDelete, objectFile.name, false);
          if (filesToDelete.length > 0) {
            filesToDelete.forEach(function (file) {
              if (fs.statSync(file).isFile())
                fs.unlinkSync(file);
            });
          }

          await app.service('documents').remove(objectFile.id, {});
        }
      }
    }

    var self = this;

    multipartMiddleware(req, res, function (err) {
      // req.file contains information of uploaded file
      // req.body contains information of text fields, if there were any
      console.log('2 222', req.file);
      if (req.fileValidationError) {
        res.status(422);
        return res.send({ err: 422, statusText: 'Unprocessable Entity', message: req.fileValidationError });
      }
      else if (!req.file) {
        res.status(422);
        return res.send({ err: 422, statusText: 'Unprocessable Entity', message: `Please select an ${self.typeUpload} to upload` });
      }
      else if (err instanceof multer.MulterError) {
        res.status(503);
        return res.send({ err: 503, statusText: 'Unavailable', message: err });
      }
      else if (err) {
        res.status(500);
        return res.send({ err: 500, statusText: 'General Error', message: err });
      }

      if (!self.checkImageHeaders(req.file.path, req.file, req.file.mimetype)){
        res.status(406);
        return res.send({ err: 406, statusText: 'Not Acceptable', message: 'Not accepted file! If you are here, there\'s something fishy with your file! HEAD-UP!' });
      }


      let width = req.query.w ? req.query.w : false;
      let height = req.query.h ? req.query.h : false;
      let resizedImagePath = req.file.destination + '/resized/' + width + 'x' + height + '/';

      if (!fs.existsSync(resizedImagePath)) {
        self.createPath(resizedImagePath)
      }

      const servicePaths = Object.keys(app.services);
      let isService = servicePaths.find(x => x === req.baseUrl.split('/')[1]);

      if (!isService) {
        throw new ForbiddenError('No! Yo can\'t go in! You little frog... Shuu ... Shuu...')
      }

      app.service('documents').create({
        id: null,
        name: req.file.filename,
        description: self.fileUse,
        document_type_id: 0,
        owner_user_id: req.params.user_id,
        folder: self.typeUpload === 'image' ? 'images' : 'documents',
        url: req.file.destination,
        createdBy: req.params.user_id,
        updatedBy: null,
        archivedAt: null,
        archivedBy: null
      });

      let newPathCreate;
      if (req.params.user_id && !req.params.complex_id) {
        newPathCreate = isService + '/' + req.params.user_id + '/' + self.typeUpload;
      }
      else if (req.params.complex_id) {
        newPathCreate = isService + '/' + (req.params.complex_id === 'all' ? '/' + self.typeUpload : req.params.complex_id + '/' + self.typeUpload);
      }
      else if (req.params.building_id) {
        newPathCreate = isService + '/' + (req.params.building_id === 'all' ? '/' + self.typeUpload : '/' + req.params.building_id + '/' + self.typeUpload);
      }
      else if (req.params.flat_id) {
        newPathCreate = isService + '/' + (req.params.flat_id === 'all' ? '/' + self.typeUpload : '/' + req.params.flat_id + '/' + self.typeUpload)
      }

      var optionsObj = [req.file.path, '-resize', width + 'x' + height, resizedImagePath + width + 'x' + height + '_' + req.file.filename];
      im.convert(optionsObj, function(err, stdout){
        if (err) throw err;

        res.send({url: newPathCreate + '/' + req.file.filename + '?w=' + width + '&' + 'h=' + height});
      });
    })
  }

  checkImageHeaders(path, file, fileTypeCheck) {
    var fileRead = fs.readFileSync(path);
    let encoding = 'base64';
    let encode_file = fileRead.toString('base64');

    let fileBuffer = new Buffer.alloc(8, encode_file, encoding);
    let arr = (new Uint8Array(fileBuffer)).subarray(0, 4);
    let header = '';
    for (var i = 0; i < arr.length; i++) {
      header += arr[i].toString(16);
    }

    let fileType = this.mimeType(header);
    if (!fileType) {
      throw new Error('Ooops! We have a broken image! Contact support for image ' + file.originalname + '!');
    }

    if (fileTypeCheck) {
      return fileType === fileTypeCheck
    }
    return fileType
  }

  mimeType(headerString) {
    switch (headerString) {
      case "89504e47":
        return "image/png";
      case "47494638":
        return "image/gif";
      case '25504446':
        return 'application/pdf';
      case "ffd8ffe0":
        return false;
      case "ffd8ffe1":
        return false;
      case "ffd8ffe2":
        return "image/jpeg";
      default:
        return false;
    }
  };

  getAllFiles(dirPath, fileName, dirName, arrayOfFiles) {
    var files = fs.readdirSync(dirPath);
    var self = this;
    arrayOfFiles = arrayOfFiles || [];
    dirName = dirName || false;

    files.forEach(function(file) {
      if (fs.statSync(dirPath + '/' + file).isDirectory()) {
        dirName = file;
        arrayOfFiles = self.getAllFiles(dirPath + '/' + file, fileName, dirName, arrayOfFiles)
      } else if (file === fileName || (dirName && file === (dirName + '_' + fileName))) {
        arrayOfFiles.push(dirPath + '/' + file)
      }
    });

    return arrayOfFiles
  };
};
