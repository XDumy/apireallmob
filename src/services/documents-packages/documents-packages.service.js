// Initializes the `documentsPackages` service on path `/documentsPackages`
const { documentsPackages } = require('./documents-packages.class');
const createModel = require('../../models/documents-packages.model');
const hooks = require('./documents-packages.hooks');

module.exports = function (app) {
  const options = {
    Model: createModel(app)
    // paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/documentsPackages', new documentsPackages(options, app), async function(req, res, next) {
    const result = res.data;
    if ((req.method === 'POST' || req.method === 'post') && !result.id) {
      let newData = await app.service('documentsPackages').find({
        query: {
          name: result.name,
          createdBy: result.createdBy,
          createdAt: result.createdAt,
        }
      });
      res.data.id = newData[0].id;
    }
    next();
  });

  // Get our initialized service so that we can register hooks
  const service = app.service('documentsPackages');

  service.hooks(hooks);
};
