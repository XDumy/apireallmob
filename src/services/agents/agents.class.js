const { Service } = require('feathers-sequelize');

exports.Agents = class Agents extends Service {
  find(params) {
    if (params.query && !params.query.deletedAt) {
      Object.assign(params.query, {deletedAt: null});
    }
    return super.find(params);
  }
  get(id, params) {
    return super.get(id, params);
  }
  remove(id, params) { return super.remove(id, params); }

  create(data, params) {
    if (!data.createdBy) {
      data.createdBy = params.user.id;
    }
    return super.create(data, params);
  }

  update(id, data, params) {
    return super.update(id, data, params);
  }
  patch(id, data, params) {
    if (!data.updatedBy && data.updatedAt) {
      data.updatedBy = params.user.id;
    }
    if (!data.deletedBy && data.deletedAt) {
      data.deletedBy = params.user.id;
    }
    return super.patch(id, data, params);
  }
};
