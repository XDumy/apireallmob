const { Service } = require('feathers-sequelize');

exports.Menu = class Menu extends Service {
  find(params) {
    params.sequelize = {
      group: ['slug']
    };
    return super.find(params)
  }
};
