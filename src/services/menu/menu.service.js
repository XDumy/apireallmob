// Initializes the `menu` service on path `/menu`
const { Menu } = require('./menu.class');
const createModel = require('../../models/menu.model');
const hooks = require('./menu.hooks');

module.exports = function (app) {
  const options = {
    Model: createModel(app),
    // paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  // app.use('/menu', new Menu(options, app));
  app.use('/menu', new Menu(options, app), async function (req, res, next) {
    const result = res.data;
    if ((req.method === 'POST' || req.method === 'post') && !result.id) {
      let newData = await app.service('flats').find({
        query: {
          name: result.name,
          createdBy: result.createdBy,
          createdAt: result.createdAt
        }
      });
      res.data.id = newData[0].id;
    }
    next()
  });

  // Get our initialized service so that we can register hooks
  const service = app.service('menu');

  service.hooks(hooks);
  // app.service('menu').hooks({
  //   before: {
  //     find(context) {
  //       // Get the Sequelize instance. In the generated application via:
  //       // const sequelize = context.app.get('sequelizeClient');
  //       // const { permissions } = sequelize.models;
  //       context.params.sequelize = {
  //         // include: [ { model: permissions, attributes: ['id', 'name', 'slug', 'menu_id', 'status', 'view', 'create', 'update', 'delete', 'export', 'all'], as: 'permissions' } ],
  //         // raw: false,
  //         group: ['slug']
  //       };
  //
  //       return context;
  //     }
  //   }
  // });
};
