const { authenticate } = require('@feathersjs/authentication').hooks;
const { when } = require('feathers-hooks-common');
const authorize = require('./../../hooks/abilities');

// const createEntityContactLink = require('../../hooks/contacts/entity-create-contact-link');

const modifyEntityContactLinks = require('../../hooks/contacts/modify-entity-contact-links');

const getContactEntityLinks = require('../../hooks/contacts/get-contact-entity-links');

const updateEntityContactLinks = require('../../hooks/contacts/update-entity-contact-link');

module.exports = {
  before: {
    all: [
      authenticate('jwt'),
      when(
        hook => hook.params.provider && `/${hook.path}` !== hook.app.get('authentication').path,
        authorize()
      ),
    ],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  after: {
    all: [],
    find: [getContactEntityLinks()],
    get: [],
    create: [createEntityContactLink()],
    update: [updateEntityContactLinks()],
    patch: [modifyEntityContactLinks()],
    remove: [modifyEntityContactLinks()]
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
