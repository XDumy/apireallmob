// Initializes the `buildings` service on path `/buildings`
const { Buildings } = require('./buildings.class');
const createModel = require('../../models/buildings.model');
const hooks = require('./buildings.hooks');

module.exports = function (app) {
  const options = {
    Model: createModel(app)
    // paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/buildings', new Buildings(options, app), async function(req, res, next) {
    const result = res.data;
    if ((req.method === 'POST' || req.method === 'post') && !result.id) {
      let newData = await app.service('buildings').find({
        query: {
          name: result.name,
          createdBy: result.createdBy,
          createdAt: result.createdAt,
        }
      });
      res.data.id = newData[0].id;
    }
    next();
  });

  // app.use('/buildings/:id', new Buildings(options, app), function(req, res, next) {
  //   const result = res.data;
  //   const data = result.data; // will be either `result` as an array or `data` if it is paginated
  //   console.log(data); // when `data` will be used, this should disapear
  //   next();
  // });

  // Get our initialized service so that we can register hooks
  const service = app.service('buildings');

  service.hooks(hooks);
};
