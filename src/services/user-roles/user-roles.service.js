// Initializes the `userRoles` service on path `/user-roles`
const { UserRoles } = require('./user-roles.class');
const createModel = require('../../models/users-roles.model');
// const removeFields = require('./../../hooks/remove-fields');
const hooks = require('./user-roles.hooks');

module.exports = function (app) {
  const options = {
    Model: createModel(app),
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/user-roles', new UserRoles(options, app), async function(req, res, next) {
    const result = res.data;
    if ((req.method === 'POST' || req.method === 'post') && !result.id) {
      let newData = await app.service('user-roles').find({
        query: {
          name: result.name,
          createdBy: result.createdBy,
          createdAt: result.createdAt,
        }
      });
      res.data.id = newData[0].id;
    }
    next();
  });

  // Get our initialized service so that we can register hooks
  const service = app.service('user-roles');

  service.hooks(hooks);
};
