// Initializes the `documents_types` service on path `/documents_types`
const { documents_types } = require('./documents_types.class');
const createModel = require('../../models/documents_types.model');
const hooks = require('./documents_types.hooks');

module.exports = function (app) {
  const options = {
    Model: createModel(app)
    // paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/documents_types', new documents_types(options, app), async function(req, res, next) {
    const result = res.data;
    if ((req.method === 'POST' || req.method === 'post') && !result.id) {
      let newData = await app.service('documents_types').find({
        query: {
          name: result.name,
          description: result.description,
          createdBy: result.createdBy,
          createdAt: result.createdAt
        }
      });
      res.data.id = newData[0].id;
    }
    next();
  });


  // Get our initialized service so that we can register hooks
  const service = app.service('documents_types');

  service.hooks(hooks);
  // eslint-disable-next-line no-unused-vars
  service.publish((data, hook) => {
    return app.channel(`connections/${data.to}`);
  });
};
