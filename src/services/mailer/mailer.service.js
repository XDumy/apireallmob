// Initializes the `mailer` service on path `/mailer`
// const { Mailer } = require('./mailer.class');
// const hooks = require('./mailer.hooks');

// Initializes the `/mailer` service on path `/mailer`
const hooks = require('./mailer.hooks');
const Mailer = require('feathers-mailer');
const smtpTransport = require('nodemailer-smtp-transport');

module.exports = function (app) {

  // const paginate = app.get('paginate');
  //
  // const options = {
  //   paginate
  // };

  // Initialize our service with any options it requires
  // app.use('/mailer', new Mailer(options, app));
  app.use('/mailer', Mailer(smtpTransport({
    host: 'vg10.gazduire.ro',
    port: 465,
    secure: true,
    auth: {
      user: 'florin@ebeton.ro',
      pass: 'h!tach!9'
    }
  })));

  // Get our initialized service so that we can register hooks
  const service = app.service('mailer');
  service.hooks(hooks);
};
