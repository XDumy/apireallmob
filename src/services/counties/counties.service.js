// Initializes the `residential_complexes` service on path `/residential_complexes`
const { Counties } = require('./counties.class');
const createModel = require('../../models/counties.model');
const hooks = require('./counties.hooks');

module.exports = function (app) {
  const options = {
    Model: createModel(app),
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/counties', new Counties(options, app), async function(req, res, next) {
    const result = res.data;
    if ((req.method === 'POST' || req.method === 'post') && !result.id) {
      let newData = await app.service('counties').find({
        query: {
          name: result.name,
          createdBy: result.createdBy,
          createdAt: result.createdAt,
        }
      });
      // eslint-disable-next-line require-atomic-updates
      res.data.id = newData[0].id;
    }
    next();
  });

  // Get our initialized service so that we can register hooks
  const service = app.service('counties');

  service.hooks(hooks);
  // eslint-disable-next-line no-unused-vars
  service.publish((data, hook) => {
    return app.channel(`connections/${data.to}`);
  });
};
