// Initializes the `agencies` service on path `/agencies`
const { Agencies } = require('./agencies.class');
const createModel = require('../../models/agencies.model');
const hooks = require('./agencies.hooks');

module.exports = function (app) {
  const options = {
    Model: createModel(app),
  };

  // Initialize our service with any options it requires
  app.use('/agencies', new Agencies(options, app), async function(req, res, next) {
    const result = res.data;
    if ((req.method === 'POST' || req.method === 'post') && !result.id) {
      let newData = await app.service('agencies').find({
        query: {
          name: result.name,
          createdBy: result.createdBy,
          createdAt: result.createdAt,
        }
      });
      res.data.id = newData[0].id;
    }
    next();
  });

  app.use('/agencies/:id', new Agencies(options, app), async function(req, res, next) {
    // const result = res.data;
    // const data = result.data; // will be either `result` as an array or `data` if it is paginated
    // console.log(data); // when `data` will be used, this should disappear
    next();
  });

  // Get our initialized service so that we can register hooks
  const service = app.service('agencies');

  service.hooks(hooks);
};
