// Initializes the `enums` service on path `/enums`
const { Enums } = require('./enums.class');
const createModel = require('../../models/enums.model');
const hooks = require('./enums.hooks');

module.exports = function (app) {
  const options = {
    Model: createModel(app)
    // paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/enums', new Enums(options, app), async function(req, res, next) {
    // const result = res.data;
    // if ((req.method === 'POST' || req.method === 'post') && !result.id) {
    //   let newData = await app.service('enums').find({
    //     query: {
    //       name: result.name,
    //       createdBy: result.createdBy,
    //       createdAt: result.createdAt,
    //     }
    //   });
    //   res.data.id = newData[0].id;
    // }
    next();
  });

  // app.use('/enums/:id', new Enums(options, app), function(req, res, next) {
  //   const result = res.data;
  //   const data = result.data; // will be either `result` as an array or `data` if it is paginated
  //   console.log(data); // when `data` will be used, this should disapear
  //   next();
  // });

  // Get our initialized service so that we can register hooks
  const service = app.service('enums');

  service.hooks(hooks);
};
