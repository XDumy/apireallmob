// Initializes the `companies` service on path `/companies`
const { Companies } = require('./companies.class');
const createModel = require('../../models/companies.model');
const hooks = require('./companies.hooks');

module.exports = function (app) {
  const options = {
    Model: createModel(app)
    // paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/companies', new Companies(options, app), async function(req, res, next) {
    const result = res.data;
    if ((req.method === 'POST' || req.method === 'post') && !result.id) {
      let newData = await app.service('companies').find({
        query: {
          name: result.name,
          createdBy: result.createdBy,
          createdAt: result.createdAt,
        }
      });
      res.data.id = newData[0].id;
    }
    next();
  });

  // Get our initialized service so that we can register hooks
  const service = app.service('companies');

  service.hooks(hooks);
};
