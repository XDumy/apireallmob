// Initializes the `uploads` service on path `/uploads`
const { Uploads } = require('./uploads.class');
const hooks = require('./uploads.hooks');

var path = require('path');
const fs = require('fs');

const multer = require('multer');

let appDir = path.dirname(require.main.filename);
let dir = appDir + '/uploads';

// check if exists dir uploads
// if is not create it
// if (!fs.existsSync(dir)){
//   fs.mkdirSync(dir);
// }

const getPath = dataBody => {
  let pathCreate = '';

  var createPath = function (checkFirstPath, fullPath) {
    if (!fs.existsSync(dir + checkFirstPath)) {
      fs.mkdirSync(dir + checkFirstPath);
      if (fullPath) {
        fs.mkdirSync(dir + fullPath)
      }
    } else if (fullPath) {
      if (!fs.existsSync(dir + fullPath)) {
        fs.mkdirSync(dir + fullPath)
      }
    }
  };
  if (dataBody) {
      if (dataBody.user && !dataBody.complex) {
        createPath('/users', '/users/' + dataBody.user);
        pathCreate = '/users/' + dataBody.user;
      }
      if (dataBody.complex) {
        createPath('/complex', '/complex/' + dataBody.complex);
        pathCreate = '/complex/' + dataBody.complex;

        if (dataBody.building) {
          createPath(pathCreate + '/building/', pathCreate + '/building/' + dataBody.building);
          pathCreate = pathCreate + '/building/' + dataBody.building;

          if (dataBody.flat) {
            createPath(pathCreate + '/flats/', pathCreate + '/flats/' + dataBody.flat);
            pathCreate = pathCreate + '/flats/' + dataBody.flat
          }
        }
      }

      if ( pathCreate.length === 0) return false;
    return pathCreate
  }
};

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    const destinationPath = getPath(req.body);
    dir = dir + '' + destinationPath;
    if (!fs.existsSync(dir)) {
      throw new ForbiddenError('No No No')
    }
    console.log('here is the body: -----', req.get('user'));
    cb(null, dir)
  },
  filename: function (req, file, cb) {
    cb(null, `${new Date().getTime()}_${file.originalname}`)
  }
});

const fileFilter = (req, file, cb) => {
  // reject a file if is not .png
  if (file.mimetype === 'image/png') {
    cb(null, true);
  } else {
    cb(null, false)
  }
};

const multipartMiddleware = multer({
  storage: storage,
  limits: {
    fileSize: 1024 * 1024 * 10
  },
  fileFilter: fileFilter
});

module.exports = function (app) {
  const options = {
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/uploads',
    new Uploads(options, app),
    async (req, res, next) => {
      console.log('---------------->>>>', );
      // if (method === 'POST' || method === 'PATCH') {}
      next()
    },
    multipartMiddleware.single('file'), async (req, res, next) => {
      const {method} = req;
      if (method === 'POST' || method === 'PATCH') {}
      next()
    }
  );

  // Get our initialized service so that we can register hooks
  const service = app.service('uploads');

  service.hooks(hooks);
};
