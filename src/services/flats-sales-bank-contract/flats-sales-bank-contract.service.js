// Initializes the `flats_sales_bank_contract` service on path `/flats-sales-bank-contract`
const { FlatsSalesBankContract } = require('./flats-sales-bank-contract.class');
const createModel = require('../../models/flats-sales-bank-contract.model');
const hooks = require('./flats-sales-bank-contract.hooks');

module.exports = function (app) {
  const options = {
    Model: createModel(app),
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/flats-sales-bank-contract', new FlatsSalesBankContract(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('flats-sales-bank-contract');

  service.hooks(hooks);
};
