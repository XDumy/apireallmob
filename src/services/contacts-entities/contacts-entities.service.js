// Initializes the `contacts-entities` service on path `/contacts-entities`
const { ContactsEntities } = require('./contacts-entities.class');
const createModel = require('../../models/contacts-entities.model');
const hooks = require('./contacts-entities.hooks');

module.exports = function (app) {
  const options = {
    Model: createModel(app),
    // paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/contacts-entities', new ContactsEntities(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('contacts-entities');

  service.hooks(hooks);
};
