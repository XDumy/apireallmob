const { Service } = require('feathers-sequelize');

exports.ContactsEntities = class ContactsEntities extends Service {
  create(data, params) {
    if (!data.createdBy) {
      data.createdBy = params.user.id;
    }
    if (!data.createdAt) {
      data.createdAt = new Date().getTime();
    }

    data.deletedBy = 0;
    if (!data.updatedBy) {
      data.updatedBy = params.user.id;
    }
    return super.create(data, params);
  }
  find(params) {
    if (params.query && !params.query.deletedAt) {
      Object.assign(params.query, {deletedAt: null});
    }
    return super.find(params);
  }
};
