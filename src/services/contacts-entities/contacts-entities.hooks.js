const { authenticate } = require('@feathersjs/authentication').hooks;
const { when } = require('feathers-hooks-common');
const authorize = require('./../../hooks/abilities');

const entityFindContactData = require('../../hooks/contacts/entity-find-contact-data');

module.exports = {
  before: {
    all: [
      authenticate('jwt'),
      when(
        hook => hook.params.provider && `/${hook.path}` !== hook.app.get('authentication').path,
        authorize()
      ),
    ],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  after: {
    all: [],
    find: [entityFindContactData()],
    get: [entityFindContactData()],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
