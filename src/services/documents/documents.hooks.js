const { authenticate } = require('@feathersjs/authentication').hooks;
const { when } = require('feathers-hooks-common');
const authorize = require('./../../hooks/abilities');
const { disallow } = require('feathers-hooks-common');


module.exports = {
  before: {
    all: [
      disallow('external'),
      authenticate('jwt'),
      when(
        hook => hook.params.provider && `/${hook.path}` !== hook.app.get('authentication').path,
        authorize()
      ),
    ],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
