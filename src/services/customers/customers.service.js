// Initializes the `agencies` service on path `/agencies`
const { Customers } = require('./customers.class');
const createModel = require('../../models/customers.model');
const hooks = require('./customers.hooks');

module.exports = function (app) {
  const options = {
    Model: createModel(app),
    // paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/customers', new Customers(options, app), async function(req, res, next) {
    const result = res.data;
    if ((req.method === 'POST' || req.method === 'post') && !result.id) {
      let newData = await app.service('customers').find({
        query: {
          name: result.name,
          createdBy: result.createdBy,
          createdAt: result.createdAt,
        }
      });
      res.data.id = newData[0].id;
    }
    next();
  });

  // app.use('/customers/:id', new Customers(options, app), async function(req, res, next) {
  //   const result = res.data;
  //   const data = result.data; // will be either `result` as an array or `data` if it is paginated
  //   console.log(data); // when `data` will be used, this should disappear
  //   next();
  // });

  // Get our initialized service so that we can register hooks
  const service = app.service('customers');

  service.hooks(hooks);
};
