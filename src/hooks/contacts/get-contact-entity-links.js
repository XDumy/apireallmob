// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html

/***
 * To be used at find and contact get
 ***/

// eslint-disable-next-line no-unused-vars
module.exports = (options = {}) => {
  return async context => {
    const { app, result, data, method } = context;

    const getContactLinks = async contact => {
      if (contact && contact.id) {
        const links = await app.service('/contacts-entities').find({
          query: {
            contact_id: contact.id
          }
        });
        return {
          ...contact,
          links
        };
      } else {
        return contact;
      }
    };
    if (method === 'find') {
      context.result = await Promise.all(result.map(getContactLinks));
      // console.log(result.data);
    } else {
      // eslint-disable-next-line require-atomic-updates
      context.result = await getContactLinks(result);
    }
    return context;
  };
};
