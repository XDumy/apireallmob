// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html

// eslint-disable-next-line no-unused-vars
module.exports = (options = {}) => {
  return async context => {
    const { app, data, result, params, method } = context;
    const updateEntityLink = async contact => {
      /* Daca e avem isMain in cerere mai intai curatam isMain de la restul */
      if (parseInt(data.isMain)) {
        const linksToBeUpdated = await app.service('/contacts-entities').find({
          query: {
            entity: data.entity,
            entity_id: data.entity_id,
            contact_id: {
              $ne: parseInt(contact.id)
            },
            isMain: 1
          }
        });
        let payload = {
          updatedBy: params.user.id,
          updatedAt: new Date().getTime()
        };
        if (linksToBeUpdated && linksToBeUpdated.length) {
          for (const item in linksToBeUpdated) {
            payload.isMain = 0;
            await app.service('/contacts-entities').patch(linksToBeUpdated[item].link.id, payload);
          }
        }
      }
      let dataToUpdate = {
        types: data.types,
        isMain: parseInt(data.isMain),
        updatedBy: params.user.id,
        updatedAt: new Date().getTime()
      };
      let retrievedData;
      if (parseInt(data.link.id)) {
        retrievedData = await app.service('/contacts-entities').patch(parseInt(data.link.id), dataToUpdate);
      }
      return {
        ...contact,
        link: retrievedData
      };
    };

    const deleteEntityLink = async contact => {
      if (data.deletedBy && result && result.id) {
        const links = await app.service('contacts-entities').find({
          query: {
            deletedBy: 0,
            contact_id: result.id
          }
        });
        for (let item in links) {
          if (method === 'patch') {
            let patchData = {
              deletedBy: contact.deletedBy ? contact.deletedBy : data.deletedBy,
              deletedAt: contact.deletedAt ? contact.deletedAt : data.deletedAt
            };
            await app.service('contacts-entities').patch(links[item].id, patchData);
          } else {
            if (method === 'delete') {
              await app.service('contacts-entities').delete(links[item].id);
            }
          }
        }
      }
    };
    console.log(data);
    if (data.entity !== 'contacts') {
      if (result.deletedBy) {
        context.result = deleteEntityLink(result);
      } else {
        context.result = updateEntityLink(result);
      }
    }
    return context;
  };
};
