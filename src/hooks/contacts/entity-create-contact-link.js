// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html

// eslint-disable-next-line no-unused-vars
module.exports = (options = {}) => {
  return async context => {
    const { app, data, params } = context;
    let contact = await app.service('contacts').find({
      query: {
        $limit: 1,
        $sort: {
          id: -1
        }
      }});
    let addContactLink = async contact => {
      if (parseInt(data.isMain)) {
        const linksToBeUpdated = await app.service('/contacts-entities').find({
          query: {
            entity: data.entity,
            entity_id: data.entity_id,
            isMain: 1,
          }
        });
        let payload = {
          updatedBy: params.user.id,
          updatedAt: new Date().getTime()
        };
        if (linksToBeUpdated && linksToBeUpdated.length) {
          for (const item in linksToBeUpdated) {
            payload.isMain = 0;
            await app.service('/contacts-entities').patch(linksToBeUpdated[item].link.id, payload);
          }
        }
      }
      const entityLine = {
        contact_id: parseInt(contact.data[0].id),
        entity: data.entity,
        entity_id: parseInt(data.entity_id),
        types: data.types,
        isMain: parseInt(data.isMain),
        deletedBy: 0,
        createdBy: parseInt(params.user.id),
        createdAt: new Date().getTime(),
        updatedBy: parseInt(params.user.id),
        updatedAt: new Date().getTime()
      };

      app.service('contacts-entities').create(entityLine);
      return {
        entityLine
      };
    };

    // eslint-disable-next-line require-atomic-updates
    context.result = await addContactLink(contact);
    return context;
  };
};
