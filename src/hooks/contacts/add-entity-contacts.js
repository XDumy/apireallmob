// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html

// eslint-disable-next-line no-unused-vars
module.exports = (options = {}) => {
  return async context => {
    const { app, result, data } = context;
    const entityType = data.entity ? data.entity : 'residential_complexes';
    const contactList = [];
    const addContact = async parentEntity => {
      if (parentEntity.id) {
        const contactsEntities = await app.service('contacts-entities').find({
          query: {
            entity: entityType,
            entity_id: parentEntity.id,
            deletedBy: 0
          }
        });

        for (let item in contactsEntities) {
          if (parseInt(contactsEntities[item].contact_id)) {
            let temp = await app.service('contacts').get(parseInt(contactsEntities[item].contact_id));
            contactList[item] = {};
            contactList[item].id = contactsEntities[item].contact_id;
            contactList[item].isMain = contactsEntities[item].isMain;
            contactList[item].entity = contactsEntities[item].entity;
            contactList[item].name = temp.name;
            contactList[item].phone01 = temp.phone01;
            contactList[item].phone02 = temp.phone02;
            contactList[item].email = temp.email;
            contactList[item].title = temp.title;
            contactList[item].job_title = temp.job_title;
            contactList[item].types = contactsEntities[item].types;
          }
        }
      }
      return {
        ...parentEntity,
        contactList
      };
    };
    // eslint-disable-next-line require-atomic-updates
    context.result = await addContact(result);
    return context;
  };
};
