// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html

/* Used when getting contacts */
// eslint-disable-next-line no-unused-vars
module.exports = (options = {}) => {
  return async context => {
    let { result } = context;
    const { app, method, params } = context;
    const createContact = async entityObject => {
      const contact = await app.service('/contacts').get(parseInt(entityObject.contact_id));
      return {
        ...contact,
        link: entityObject
      };
    };

    if (method === 'find') {
      // eslint-disable-next-line require-atomic-updates
      context.result = await Promise.all(result.map(createContact));
    } else {
      // eslint-disable-next-line require-atomic-updates
      context.result = await createContact(result);
    }
    return context;
  };
};
