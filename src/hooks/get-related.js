// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html

// eslint-disable-next-line no-unused-vars
module.exports = function (options = {}) {
  return async context => {
    // const sequelize = context.app.get('sequelizeClient');
    // const { users_roles, roles, permissions, users, menu } = sequelize.models;
    // // console.log('==>', context);
    // if (context.app.params && context.app.params === 'login' ) {
    //   context.params.sequelize = {
    //     include: [
    //       { model: users_roles, attributes: ['user_id', 'role_id', 'residential_complex_id'], as: 'userRoles', where: { status: 1 }, include: [
    //           { model: roles, attributes: ['id', 'name', 'description'], as: 'role', include: [
    //               { model: permissions, attributes: ['id', 'name', 'slug', 'menu_id', 'status', 'view', 'create', 'update', 'delete', 'export', 'all'], as: 'permissions', include: [
    //                   { model: menu, attributes: ['id', 'name', 'slug', 'description', 'parent_id', 'status', 'for_permission', 'visible', 'position', 'icon'], as: 'menu', where: { status: 1 }}
    //               ]},
    //           ]},
    //       ]},
    //     ],
    //     raw: false,
    //   };
    // } else {
    //   context.params.sequelize = {
    //     include: [
    //       { model: users_roles, attributes: ['user_id', 'role_id', 'residential_complex_id'], as: 'userRoles', include: [
    //           { model: roles, attributes: ['name', 'description'] },
    //         ]},
    //     ],
    //     raw: false,
    //   };
    // }

    return context;
  };
};
