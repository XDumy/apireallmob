// See http://docs.sequelizejs.com/en/latest/docs/models-definition/
// for more of what you can do here.
const Sequelize = require('sequelize');
const DataTypes = Sequelize.DataTypes;

module.exports = function (app) {
  const sequelizeClient = app.get('sequelizeClient');
  const contacts = sequelizeClient.define('contacts', {
    id: {
      primaryKey: true,
      type: DataTypes.INTEGER,
      unique: true
    },
    name: {
      type: DataTypes.STRING,
      length: 1024,
      allowNull: false,
      unique: false
    },
    gender: {
      type: DataTypes.STRING,
      length: 1,
      allowNull: true,
      unique: false
    },
    title: {
      type: DataTypes.STRING,
      length: 10,
      allowNull: true,
      unique: false
    },
    job_title: {
      type: DataTypes.STRING,
      length: 1024,
      allowNull: true,
      unique: false
    },
    phone01: {
      type: DataTypes.STRING,
      length: 1024,
      allowNull: true,
      unique: false
    },
    phone02: {
      type: DataTypes.STRING,
      length: 1024,
      allowNull: true,
      unique: false
    },
    email: {
      type: DataTypes.STRING,
      length: 1024,
      allowNull: true,
      unique: true
    },
    language: {
      type: DataTypes.STRING,
      length: 2,
      allowNull: true,
      unique: false
    },
    country: {
      type: DataTypes.STRING,
      length: 3,
      allowNull: true,
      unique: false
    },
    createdBy: {
      type: DataTypes.INTEGER,
      length: 11,
      allowNull: false,
      default: 0,
    },
    createdAt: {
      type: DataTypes.DATE,
      allowNull: false
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: true
    },
    updatedBy: {
      type: DataTypes.INTEGER,
      length: 11,
      allowNull: true
    },
    deletedAt: {
      type: DataTypes.DATE,
      allowNull: true,
      unique: false,
      default: null
    },
    deletedBy: {
      type: DataTypes.INTEGER,
      length: 11,
      allowNull: true
    }
  }, {
    hooks: {
      beforeCount(options) {
        options.raw = true;
      }
    }
  });

  // eslint-disable-next-line no-unused-vars
  contacts.associate = function (models) {
    // Define associations here
    // See http://docs.sequelizejs.com/en/latest/docs/associations/
  };

  return contacts;
};
