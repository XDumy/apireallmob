// See http://docs.sequelizejs.com/en/latest/docs/models-definition/
// for more of what you can do here.
const Sequelize = require('sequelize');
const DataTypes = Sequelize.DataTypes;

module.exports = function (app) {
  const sequelizeClient = app.get('sequelizeClient');
  const agencies = sequelizeClient.define('agencies', {
    id: {
      primaryKey: true,
      type: DataTypes.INTEGER,
      unique: true,
    },
    name: {
      type: DataTypes.STRING,
      length: 100,
      allowNull: true,
      unique: false,
    },
    company_id: {
      type: DataTypes.INTEGER,
      length: 11,
      allowNull: true,
      unique: false,
    },
    county_id: {
      type: DataTypes.INTEGER,
      length: 1,
      allowNull: true,
      unique: false,
    },
    settlement_id: {
      type: DataTypes.INTEGER,
      length: 11,
      allowNull: true,
      unique: false,
    },
    address_body: {
      type: DataTypes.STRING,
      length: 1024,
      allowNull: true,
      unique: false,
    },
    postal_code: {
      type: DataTypes.STRING,
      length: 20,
      allowNull: true,
      unique: false,
    },
    phone01: {
      type: DataTypes.STRING,
      length: 1024,
      allowNull: true,
    },
    phone02: {
      type: DataTypes.STRING,
      length: 1024,
      allowNull: true,
    },
    email: {
      type: DataTypes.STRING,
      length: 1024,
      allowNull: true,
    },
    documents: {
      type: DataTypes.STRING,
      length: 1024,
      allowNull: true,
      unique: false,
    },
    inactive: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    createdAt: {
      type: DataTypes.DATE,
      length: 11,
      unique: false,
    },
    createdBy: {
      type: DataTypes.INTEGER,
      length: 11,
      allowNull: false
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: true
    },
    updatedBy: {
      type: DataTypes.INTEGER,
      length: 11,
      allowNull: true
    },
    deletedAt: {
      type: DataTypes.DATE,
      allowNull: true
    },
    deletedBy: {
      type: DataTypes.INTEGER,
      length: 11,
      allowNull: true
    }
  }, {
    hooks: {
      beforeCount(options) {
        options.raw = true;
      }
    }
  });

  // eslint-disable-next-line no-unused-vars
  agencies.associate = function (models) {
    // Define associations here
    // See http://docs.sequelizejs.com/en/latest/docs/associations/
    // users.hasMany(models.users_roles, {
    //   as: 'UserRoles',
    //   foreignKey: 'user_id',
    //   targetKey: 'id'
    // });
    // const { users_roles, roles, permissions, menu } = models;
    // settlements.hasMany(users_roles, {
    //   foreignKey: 'settlement_id',
    //   sourceKey: 'id',
    //   as: 'userRoles'
    // });
  };

  return agencies;
};
