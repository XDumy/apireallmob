// See http://docs.sequelizejs.com/en/latest/docs/models-definition/
// for more of what you can do here.
const Sequelize = require('sequelize');
const DataTypes = Sequelize.DataTypes;

module.exports = function (app) {
  const sequelizeClient = app.get('sequelizeClient');
  const flats = sequelizeClient.define('flats', {
    id: {
      primaryKey: true,
      type: DataTypes.INTEGER,
      length: 11,
      unique: true,
    },
    name: {
      type: DataTypes.STRING,
      length: 1024,
      allowNull: true,
      unique: true,
    },
    description: {
      type: DataTypes.STRING,
      length: 1024,
      allowNull: true,
      unique: false,
    },
    flat_type_id: {
      type: DataTypes.INTEGER,
      length: 11,
      unique: false,
    },
    residential_complex_id: {
      type: DataTypes.INTEGER,
      length: 11,
      unique: false,
    },
    building_id: {
      type: DataTypes.INTEGER,
      length: 11,
      unique: false,
      allowNull: true,
    },
    statusc_id: {
      type: DataTypes.INTEGER,
      length: 11,
      unique: false,
    },
    statusv_id: {
      type: DataTypes.INTEGER,
      length: 11,
      unique: false,
    },
    no_rooms: {
      type: DataTypes.INTEGER,
      length: 11,
      unique: false,
    },
    floor: {
      type: DataTypes.INTEGER,
      length: 11,
      unique: false,
    },
    area_sqm: {
      type: DataTypes.FLOAT,
      length: 11,
      unique: false,
    },
    no_parking_indoor: {
      type: DataTypes.INTEGER,
      length: 11,
      unique: false,
    },
    no_parking_outdoor: {
      type: DataTypes.INTEGER,
      length: 11,
      unique: false,
    },
    price_net: {
      type: DataTypes.FLOAT,
      allowNull: true,
      unique: false,
    },
    vat: {
      type: DataTypes.INTEGER,
      allowNull: true,
      unique: false,
    },
    currency: {
      type: DataTypes.STRING,
      allowNull: true,
      unique: false,
    },
    documents: {
      type: DataTypes.TEXT,
      allowNull: true,
      unique: false,
    },
    createdAt: {
      type: DataTypes.DATE,
      length: 11,
      unique: false,
    },
    createdBy: {
      type: DataTypes.INTEGER,
      length: 11,
      allowNull: false,
      unique: false,
      default: 0,
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: true,
      unique: false,
      default: null
    },
    updatedBy: {
      type: DataTypes.INTEGER,
      length: 11,
      allowNull: true,
      unique: false,
      default: null
    },
    deletedAt: {
      type: DataTypes.DATE,
      allowNull: true,
      unique: false,
      default: null
    },
    deletedBy: {
      type: DataTypes.INTEGER,
      length: 11,
      allowNull: true,
      unique: false,
      default: null
    }
  }, {
    hooks: {
      beforeCount(options) {
        options.raw = true;
      }
    }
  });

  // eslint-disable-next-line no-unused-vars
  flats.associate = function (models) {
    // Define associations here
    // See http://docs.sequelizejs.com/en/latest/docs/associations/
    // const { flats, flats_sales } = models;
    //
    // flats.hasMany(buildings, { foreignKey: '_id', sourceKey: 'id', as: 'building_flats' });
    // flats.belongsToMany(buildings, { foreignKey: 'statusc_id', as: 'buildings', through: { model: 'buildings', foreignKey: 'id' } });
    // flats.belongsToMany(buildings, { foreignKey: 'statusv_id', as: 'buildings', through: { model: 'buildings', foreignKey: 'id' } });
  };

  return flats;
};
