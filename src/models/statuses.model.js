// See http://docs.sequelizejs.com/en/latest/docs/models-definition/
// for more of what you can do here.
const Sequelize = require('sequelize');
const DataTypes = Sequelize.DataTypes;

module.exports = function (app) {
  const sequelizeClient = app.get('sequelizeClient');
  const statuses = sequelizeClient.define('statuses', {
    id: {
      primaryKey: true,
      type: DataTypes.INTEGER,
      length: 11,
      unique: true
    },
    name: {
      type: DataTypes.STRING,
      length: 1024,
      allowNull: true,
      unique: false
    },
    description: {
      type: DataTypes.STRING,
      length: 1024,
      allowNull: true,
      unique: false
    },
    for_construction: {
      type: DataTypes.TINYINT,
      length: 1,
      allowNull: true
    },
    inactive: {
      type: DataTypes.TINYINT,
      length: 1,
      allowNull: true
    },
    order: {
      type: DataTypes.INTEGER,
      length: 10,
      allowNull: true
    },
    parent_id: {
      type: DataTypes.INTEGER,
      length: 11,
      allowNull: true
    },
    automatic: {
      type: DataTypes.TINYINT,
      length: 1,
      allowNull: true,
      default: 1
    },
    is_start: {
      type: DataTypes.TINYINT,
      length: 1,
      allowNull: true,
      default: 1
    },
    pending: {
      type: DataTypes.TINYINT,
      length: 1,
      allowNull: true,
      default: 1
    },
    is_closed: {
      type: DataTypes.TINYINT,
      length: 1,
      allowNull: true,
      default: 1
    },
    with_notification: {
      type: DataTypes.TINYINT,
      length: 1,
      allowNull: true,
      default: 1
    },
    createdAt: {
      type: DataTypes.DATE,
      allowNull: false,
      unique: false,
      default: 0
    },
    createdBy: {
      type: DataTypes.INTEGER,
      length: 11,
      allowNull: false,
      unique: false,
      default: 0,
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: true,
      unique: false,
      default: null
    },
    updatedBy: {
      type: DataTypes.INTEGER,
      length: 11,
      allowNull: true,
      unique: false,
      default: null
    },
    deletedAt: {
      type: DataTypes.DATE,
      allowNull: true,
      unique: false,
      default: null
    },
    deletedBy: {
      type: DataTypes.INTEGER,
      length: 11,
      allowNull: true,
      unique: false,
      default: null
    }
  }, {
    hooks: {
      beforeCount(options) {
        options.raw = true;
      }
    }
  });

  // eslint-disable-next-line no-unused-vars
  statuses.associate = function (models) {
    // Define associations here
    // See http://docs.sequelizejs.com/en/latest/docs/associations/
  };

  return statuses;
};
