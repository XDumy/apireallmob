// See http://docs.sequelizejs.com/en/latest/docs/models-definition/
// for more of what you can do here.
const Sequelize = require('sequelize');
const DataTypes = Sequelize.DataTypes;

module.exports = function (app) {
  const sequelizeClient = app.get('sequelizeClient');
  const agents = sequelizeClient.define('agents', {

    id: {
      primaryKey: true,
      type: DataTypes.INTEGER,
      length: 11,
      unique: true,
    },
    name: {
      type: DataTypes.STRING,
      length: 1024,
      allowNull: true
    },
    title: {
      type: DataTypes.STRING,
      length: 1024,
      allowNull: true
    },
    contact_id: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    manager_agent_id: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    bkp_agent_id: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    external_agent: {
      type: DataTypes.TINYINT,
      length: 1,
      allowNull: true
    },
    inactive: {
      type: DataTypes.TINYINT,
      length: 1,
      allowNull: true
    },
    createdBy: {
      type: DataTypes.INTEGER,
      length: 11,
      allowNull: false,
      unique: false,
      default: 0,
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: true,
      unique: false,
      default: null
    },
    updatedBy: {
      type: DataTypes.INTEGER,
      length: 11,
      allowNull: true,
      unique: false,
      default: 0
    },
    deletedAt: {
      type: DataTypes.DATE,
      allowNull: true,
      unique: false,
      default: null
    },
    deletedBy: {
      type: DataTypes.INTEGER,
      length: 11,
      allowNull: true,
      unique: false,
      default: 0
    }
  }, {
    hooks: {
      beforeCount(options) {
        options.raw = true;
      }
    }
  });

  // eslint-disable-next-line no-unused-vars
  agents.associate = function (models) {
    // Define associations here
    // See http://docs.sequelizejs.com/en/latest/docs/associations/
    // const { contacts, flats_sales } = models;
    //
    // contacts.hasMany(flats, { foreignKey: 'block_id', sourceKey: 'id', as: 'building_flats' });
    // contacts.belongsToMany(flats_sales, { foreignKey: 'flat_id', as: 'flats_sales', through: { model: 'flats', foreignKey: 'id' } });

  };

  return agents;
};
