// See http://docs.sequelizejs.com/en/latest/docs/models-definition/
// for more of what you can do here.
const Sequelize = require('sequelize');
const DataTypes = Sequelize.DataTypes;

module.exports = function (app) {
  const sequelizeClient = app.get('sequelizeClient');
  const buildings = sequelizeClient.define('buildings', {

    id: {
      primaryKey: true,
      type: DataTypes.INTEGER,
      length: 11,
      unique: true,
    },
    name: {
      type: DataTypes.STRING,
      length: 100,
      allowNull: true,
      unique: false,
    },
    description: {
      type: DataTypes.STRING,
      length: 100,
      allowNull: true,
      unique: false,
    },
    residential_complex_id: {
      type: DataTypes.INTEGER,
      length: 11,
      unique: false,
    },
    no_floors: {
      type: DataTypes.STRING,
      length: 100,
      allowNull: false,
    },
    no_apartments: {
      type: DataTypes.INTEGER,
      length: 11,
      allowNull: false,
    },
    statusc_id: {
      type: DataTypes.INTEGER,
      length: 11,
      allowNull: false,
    },
    statusv_id: {
      type: DataTypes.INTEGER,
      length: 11,
      allowNull: false,
    },
    indoor_parking_places: {
      type: DataTypes.INTEGER,
      length: 11,
      allowNull: false,
    },
    outdoor_parking_places: {
      type: DataTypes.INTEGER,
      length: 11,
      allowNull: false,
    },
    documents: {
      type: DataTypes.TEXT,
      allowNull: true,
    },
    createdAt: {
      type: DataTypes.DATE,
      length: 11,
      unique: false,
    },
    createdBy: {
      type: DataTypes.INTEGER,
      length: 11,
      allowNull: false,
      unique: false,
      default: 0,
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: true,
      unique: false,
      default: null
    },
    updatedBy: {
      type: DataTypes.INTEGER,
      length: 11,
      allowNull: true,
      unique: false,
      default: null
    },
    deletedAt: {
      type: DataTypes.DATE,
      allowNull: true,
      unique: false,
      default: null
    },
    deletedBy: {
      type: DataTypes.INTEGER,
      length: 11,
      allowNull: true,
      unique: false,
      default: null
    }
  }, {
    hooks: {
      beforeCount(options) {
        options.raw = true;
      }
    }
  });

  // eslint-disable-next-line no-unused-vars
  buildings.associate = function (models) {
    // Define associations here
    // See http://docs.sequelizejs.com/en/latest/docs/associations/
    // const { flats, flats_sales } = models;
    //
    // buildings.hasMany(flats, { foreignKey: 'block_id', sourceKey: 'id', as: 'building_flats' });
    // buildings.belongsToMany(flats_sales, { foreignKey: 'flat_id', as: 'flats_sales', through: { model: 'flats', foreignKey: 'id' } });
  };

  return buildings;
};
