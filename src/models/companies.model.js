// See http://docs.sequelizejs.com/en/latest/docs/models-definition/
// for more of what you can do here.
const Sequelize = require('sequelize');
const DataTypes = Sequelize.DataTypes;

module.exports = function (app) {
  const sequelizeClient = app.get('sequelizeClient');
  const companies = sequelizeClient.define('companies', {
    id: {
      primaryKey: true,
      type: DataTypes.INTEGER,
      unique: true,
    },
    name: {
      type: DataTypes.STRING,
      length: 1024,
      allowNull: false,
      unique: true,
    },
    juridical_type: {
      type: DataTypes.STRING,
      length: 4,
      allowNull: true
    },
    legal_name: {
      type: DataTypes.STRING,
      length: 1024,
      allowNull: true
    },
    cui: {
      type: DataTypes.STRING,
      length: 100,
      allowNull: true,
      unique: true
    },
    attribute: {
      type: DataTypes.STRING,
      length: 10,
      allowNull: true
    },
    reg_no: {
      type: DataTypes.STRING,
      length: 100,
      allowNull: true
    },
    phone: {
      type: DataTypes.STRING,
      length: 100,
      allowNull: true
    },
    email: {
      type: DataTypes.STRING,
      length: 1024,
      allowNull: true
    },
    documents: {
      type: DataTypes.STRING,
      length: 1024,
      allowNull: true
    },
    bank_name: {
      type: DataTypes.STRING,
      length: 1024,
      allowNull: true
    },
    bank_account: {
      type: DataTypes.STRING,
      length: 100,
      allowNull: true
    },
    currency: {
      type: DataTypes.STRING,
      length: 3,
      allowNull: true
    },
    createdAt: {
      type: DataTypes.DATE,
      length: 11,
      allowNull: false
    },
    createdBy: {
      type: DataTypes.INTEGER,
      length: 11,
      allowNull: false
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: true
    },
    updatedBy: {
      type: DataTypes.INTEGER,
      length: 11,
      allowNull: true
    },
    deletedAt: {
      type: DataTypes.DATE,
      allowNull: true
    },
    deletedBy: {
      type: DataTypes.INTEGER,
      length: 11,
      allowNull: true
    }
  }, {
    hooks: {
      beforeCount(options) {
        options.raw = true;
      }
    }
  });

  // eslint-disable-next-line no-unused-vars
  companies.associate = function (models) {
    // Define associations here
    // See http://docs.sequelizejs.com/en/latest/docs/associations/
  };

  return companies;
};
