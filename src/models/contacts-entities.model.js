// See http://docs.sequelizejs.com/en/latest/docs/models-definition/
// for more of what you can do here.
const Sequelize = require('sequelize');
const DataTypes = Sequelize.DataTypes;

module.exports = function (app) {
  const sequelizeClient = app.get('sequelizeClient');
  const contactsEntities = sequelizeClient.define('contacts_entites', {
    id: {
      primaryKey: true,
      type: DataTypes.INTEGER,
      length: 11,
      unique: true,
    },
    contact_id: {
      type: DataTypes.INTEGER,
      length: 11,
    },
    entity: {
      type: DataTypes.ENUM,
      values: ['residential_complexes', 'agencies', 'flats', 'buildings', 'customers']
    },
    entity_id: {
      type: DataTypes.INTEGER
    },
    types: {
      type: DataTypes.STRING
    },
    isMain: {
      type: DataTypes.INTEGER
    },
    createdAt: {
      type: DataTypes.DATE,
      length: 11,
      unique: false,
    },
    createdBy: {
      type: DataTypes.INTEGER,
      length: 11,
      allowNull: false,
      unique: false,
      default: 0,
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: true,
      unique: false,
      default: null
    },
    updatedBy: {
      type: DataTypes.INTEGER,
      length: 11,
      allowNull: true,
      unique: false,
      default: 0
    },
    deletedAt: {
      type: DataTypes.DATE,
      allowNull: true,
      unique: false,
      default: null
    },
    deletedBy: {
      type: DataTypes.INTEGER,
      length: 11,
      allowNull: true,
      unique: false,
      default: 0
    }

  }, {
    hooks: {
      beforeCount(options) {
        options.raw = true;
      }
    }
  });

  // eslint-disable-next-line no-unused-vars
  contactsEntities.associate = function (models) {
    // Define associations here
    // See http://docs.sequelizejs.com/en/latest/docs/associations/
  };

  return contactsEntities;
};
