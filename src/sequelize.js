const Sequelize = require('sequelize');

module.exports = function (app) {
  const connectionString = app.get('mysql');
  const sequelize = new Sequelize(connectionString.database, connectionString.username, connectionString.password, {
    host: connectionString.host,
    port: connectionString.port,
    dialect: 'mysql',
    logging: false,
    pool: {
      max: 5,
      min: 0,
      idle: 10000,
    },
    // dialectOptions: {
    //   connectTimeout: 1000
    // }, // mariadb connector option
    define: {
      freezeTableName: true
    }
  });
  const oldSetup = app.setup;

  app.set('sequelizeClient', sequelize);

  app.setup = function (...args) {
    const result = oldSetup.apply(this, args);

    // Set up data relationships
    const models = sequelize.models;
    Object.keys(models).forEach(name => {
      if ('associate' in models[name]) {
        models[name].associate(models);
      }
    });

    sequelize.authenticate().then(() => {
        console.log('Connection has been established successfully.');
      }).catch((err) => {
        console.log('Unable to connect to the database:', err);
      });
    // Sync to the database
    app.set('sequelizeSync', sequelize.sync());

    setInterval(function () {
      sequelize.query('SELECT SLEEP(1);', function (err, rows, fields) {
        if (err) throw err;
        console.log('sleep');
      });
    }, 2000);
    // setInterval(function () {
    //   pool.query('SELECT SLEEP(1);', function (err, rows, fields) {
    //     if (err) throw err;
    //     console.log('sleep');
    //   });
    // }, 2000);

    return result;
  };
};
